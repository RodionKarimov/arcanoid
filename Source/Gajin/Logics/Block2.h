// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Net/UnrealNetwork.h"
#include "DestructibleActor.h"
#include "Block2.generated.h"

/**
 * 
 */
UCLASS()
class GAJIN_API ABlock2 : public ADestructibleActor
{
	GENERATED_BODY()



	ABlock2 ( const FObjectInitializer & ObjectInitializer );

	UPROPERTY ( Transient, ReplicatedUsing = OnRep_bExploded )
	bool m_bExploded = false;

	UPROPERTY(EditDefaultsOnly, Category = Config)
	float m_fLifeSpanAfterExplosion = 2.0f;

	UPROPERTY(EditDefaultsOnly, Category = Config)
	float m_fExplosionDamage = 0.0f;

	UPROPERTY(EditDefaultsOnly, Category = Config)
	float m_fExplosionRadius = 0.0f;

	UPROPERTY(EditDefaultsOnly, Category=Config)
		TSubclassOf<UDamageType> m_classDamageType;

	/** initial setup */
	virtual void PostInitializeComponents() override;

	/** [client] explosion happened */
	UFUNCTION ()
	void OnRep_bExploded ();

	UFUNCTION()
	void OnActorFracture_Handler ( const FVector& HitPoint, const FVector& HitDirection );

	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit);



public:
	UFUNCTION(BlueprintImplementableEvent)
	void OnPlayExplosionEffects ();
	
};
