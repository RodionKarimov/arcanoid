// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/TriggerBox.h"
#include "TriggerBox_Platform.generated.h"

/**
 * 
 */
UCLASS()
class GAJIN_API ATriggerBox_Platform : public ATriggerBox
{
	GENERATED_BODY()

	ATriggerBox_Platform ( const FObjectInitializer& ObjectInitializer );

	UPROPERTY(EditDefaultsOnly, Category=Config)
   	TSubclassOf<AActor> m_classBall;

	UFUNCTION()
	void OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);
	
};
