// Fill out your copyright notice in the Description page of Project Settings.

#include "TriggerBox_Platform.h"
#include "Components/ShapeComponent.h"
#include "Ball.h"



ATriggerBox_Platform :: ATriggerBox_Platform ( const FObjectInitializer& ObjectInitializer )
	: Super(ObjectInitializer) {
	bReplicates                            = true;
	bAlwaysRelevant                        = true;

	GetCollisionComponent () -> OnComponentBeginOverlap.AddDynamic(this, &ATriggerBox_Platform::OnOverlapBegin);       // set up a notification for when this component overlaps something
	GetCollisionComponent () -> OnComponentEndOverlap.AddDynamic(this, &ATriggerBox_Platform::OnOverlapEnd);

}

void ATriggerBox_Platform :: OnOverlapBegin(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) {

	UE_LOG ( LogTemp, Warning, TEXT ( "ATriggerBox_Platform.OnOverlapBegin () : Actor entered trigger : %s ." ), * OtherActor -> GetClass () -> GetName () );

	// if ( OtherActor -> GetClass () == m_classBall ) {
	if ( Cast < ABall > ( OtherActor ) != nullptr ) {
		UE_LOG ( LogTemp, Warning, TEXT ( "ATriggerBox_Platform.OnOverlapBegin () : Ball entered trigger." ) );
		Cast < ABall > ( OtherActor ) -> CollisionComp -> AddImpulse ( FVector ( 0.0f, 0.0f, 4000.0f ), NAME_None, true );
	} //-if

	// if ( OtherActor && ( OtherActor != this ) && OtherComp && bActive && ! IsObjectPickedUp &&
	//      OtherActor -> GetClass () -> IsChildOf ( AGunBlazeCharacter :: StaticClass () ) &&
	//      ( GEngine -> GetNetMode ( GetWorld () ) != NM_Client ) ) {
	//   //UE_LOG ( LogTemp, Warning, TEXT ( "AactorBoxTrigger_CameraArea.OnOverlapBegin () : 2 : %s ." ), * OtherActor -> GetClass () -> GetName () );

	//   PickUpObject ( Cast < AGunBlazeCharacter > ( OtherActor ) );

	// } //-if

}

void ATriggerBox_Platform :: OnOverlapEnd(class UPrimitiveComponent* OverlappedComp, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) {

	UE_LOG ( LogTemp, Warning, TEXT ( "ATriggerBox_Platform.OnOverlapEnd () : Actor exited trigger : %s ." ), * OtherActor -> GetClass () -> GetName () );

	/*if ( OtherActor && (OtherActor != this) && OtherComp && OtherActor -> GetClass () -> IsChildOf ( AGunBlazeCharacter :: StaticClass () ) &&
	     ( GEngine -> GetNetMode ( GetWorld () ) != NM_Client ) ) {
	  aActorsInsideTrigger.Remove ( ( AGunBlazeCharacter * ) OtherActor );

	  if ( ( ( AGunBlazeCharacter * ) OtherActor != NULL ) &&
	       ( ( AGunBlazePlayerState * ) ( ( AGunBlazeCharacter * ) OtherActor ) -> PlayerState != NULL ) ) {
	    ( ( AGunBlazePlayerState * ) ( ( AGunBlazeCharacter * ) OtherActor ) -> PlayerState ) -> SetInArea (
	      false, GetUniqueID (), 1.0f );

	    ( ( AGunBlazePlayerController * ) ( ( ( AGunBlazeCharacter * ) OtherActor ) -> Controller ) ) -> Notify_InsideCameraAreaTrigger ( false );

	  } //-if

	} //-if*/

}
