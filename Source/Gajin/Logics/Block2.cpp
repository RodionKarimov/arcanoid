// Fill out your copyright notice in the Description page of Project Settings.

#include "Block2.h"
#include "DestructibleComponent.h"
#include "Kismet/GameplayStatics.h"



ABlock2 :: ABlock2 ( const FObjectInitializer & ObjectInitializer )
	: Super ( ObjectInitializer )
{
	//OnActorFracture.AddUniqueDynamic(this, &ABlock2::OnActorFracture_Handler);
	//GetDestructibleComponent()->OnComponentFracture.AddDynamic(this, &ABlock2::OnActorFracture_Handler);
	GetDestructibleComponent()->SetIsReplicated(true);

	GetDestructibleComponent()->OnComponentHit.AddDynamic(this, &ABlock2::OnHit);

	bReplicates = true;
	bAlwaysRelevant = true;
}

void ABlock2 :: PostInitializeComponents() {
	Super :: PostInitializeComponents ();
	//OnActorFracture.AddUniqueDynamic(this, &ABlock2::OnActorFracture_Handler);
	if ( ! GetDestructibleComponent()->OnComponentFracture.IsBound () )
		GetDestructibleComponent()->OnComponentFracture.AddDynamic(this, &ABlock2::OnActorFracture_Handler);
}

void ABlock2::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
    Super::GetLifetimeReplicatedProps(OutLifetimeProps);
    DOREPLIFETIME(ABlock2, m_bExploded);
}

void ABlock2 :: OnRep_bExploded () {
	UE_LOG(LogTemp, Warning, TEXT("ABlock2.OnRep_bExploded () : %i."), int ( m_bExploded ) );
	//DrawDebugSphere ( GetWorld (), GetDestructibleComponent () -> GetWorldLocation () + FVector ( 0.0f, 0.0f, 10.0f ), 20.0f, 20, FColor::Red, false, 10, 10 );
	//UGameplayStatics::ApplyRadialDamage ( this, 1000.0f, GetDestructibleComponent () -> GetWorldLocation () + FVector ( 0.0f, 0.0f, 10.0f ), 50.0, classDamageType, TArray < AActor * > (), this, nullptr);	
  	//FDamageEvent                          structureDamageEvent ( classDamageType );
	//GetDestructibleComponent () -> ReceiveComponentDamage ( 1000.0f, structureDamageEvent, nullptr, this );
	if ( m_fExplosionDamage > 0.0f )
		GetDestructibleComponent () -> ApplyRadiusDamage ( 1000.0f, GetActorLocation () , 500.0f, 1000.0f, true );

	OnPlayExplosionEffects ();
	SetLifeSpan ( m_fLifeSpanAfterExplosion );
	GetDestructibleComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECR_Ignore);
}

void ABlock2 :: OnActorFracture_Handler ( const FVector& HitPoint, const FVector& HitDirection ) {
	TArray < AActor * > aExplosionIgnoredActors;
	//UE_LOG(LogTemp, Warning, TEXT("ABlock2.OnActorFracture_Handler () : %i."), int ( GEngine -> GetNetMode ( GetWorld () ) ) );

	if ( m_bExploded ) return;

	if ( ( GEngine -> GetNetMode ( GetWorld () ) == NM_ListenServer ) || ( GEngine -> GetNetMode ( GetWorld () ) == NM_Standalone ) ) {
		OnPlayExplosionEffects ();
	}
	
	if ( m_fExplosionDamage > 0.0f ) {
		UE_LOG(LogTemp, Warning, TEXT("ABlock2.OnActorFracture_Handler () : Inflicting damage.") );
		aExplosionIgnoredActors.Add ( this );
		UGameplayStatics::ApplyRadialDamage(this, m_fExplosionDamage, GetActorLocation () + FVector ( 0.0f, 0.0f, 50.0f ), m_fExplosionRadius, m_classDamageType, aExplosionIgnoredActors, this, nullptr);
	}

	SetLifeSpan ( m_fLifeSpanAfterExplosion );

	m_bExploded = true;
	GetDestructibleComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECR_Ignore);

}

// void ABlock2::OnImpact(const FHitResult& HitResult)
// {
// 	if ((Role == ROLE_Authority) && !bExploded)
// 	{
// 		Explode(HitResult);
// 		DisableAndDestroy();
// 	}
// }

void ABlock2::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit) {
	UE_LOG(LogTemp, Warning, TEXT("ABlock2.OnHit () : Component is hit.") );

	// if ((Role == ROLE_Authority) && !bExploded)
	// {
	// 	Explode(Hit);
	// 	DisableAndDestroy();
	// }

	GetDestructibleComponent () -> ApplyRadiusDamage ( 1000.0f, GetActorLocation () , 500.0f, 1000.0f, true );

}