// Fill out your copyright notice in the Description page of Project Settings.

#include "Ball.h"

// Sets default values
ABall::ABall(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;



	CollisionComp = ObjectInitializer.CreateDefaultSubobject<USphereComponent>(this, TEXT("SphereComp"));
    CollisionComp->InitSphereRadius(50.0f);
    //CollisionComp->AlwaysLoadOnClient = true;
    //CollisionComp->AlwaysLoadOnServer = true;
    //CollisionComp->bTraceComplexOnMove = true;
    //if ( GetNetMode () != NM_Client )
    CollisionComp->SetSimulatePhysics(true);
    //else CollisionComp->SetSimulatePhysics(false);
    //CollisionComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
    //CollisionComp->SetCollisionObjectType(COLLISION_PROJECTILE);
    //CollisionComp->SetCollisionResponseToAllChannels(ECR_Ignore);
    //CollisionComp->SetCollisionResponseToChannel(ECC_WorldStatic, ECR_Block);
    //CollisionComp->SetCollisionResponseToChannel(ECC_WorldDynamic, ECR_Block);
    //CollisionComp->SetCollisionResponseToChannel(ECC_Pawn, ECR_Block);
    CollisionComp->SetCollisionProfileName(FName("BlockAll"));
    CollisionComp->BodyInstance.bUseCCD = true;
    RootComponent = CollisionComp;

	StaticMesh = ObjectInitializer.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("StaticMesh"));
    //Mesh->MeshComponentUpdateFlag = EMeshComponentUpdateFlag::OnlyTickPoseWhenRendered;
    //Mesh->bChartDistanceFactor = true;
    StaticMesh->bReceivesDecals = true;
    StaticMesh->CastShadow = true;
    // StaticMesh->SetCollisionObjectType(ECC_WorldDynamic);
    // StaticMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
    // StaticMesh->SetCollisionResponseToAllChannels(ECR_Block);
    StaticMesh->SetupAttachment(CollisionComp);

    // StaticMesh->SetSimulatePhysics(true);
    // //else CollisionComp->SetSimulatePhysics(false);
    // //CollisionComp->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
    // //CollisionComp->SetCollisionObjectType(COLLISION_PROJECTILE);
    // //CollisionComp->SetCollisionResponseToAllChannels(ECR_Ignore);
    // //CollisionComp->SetCollisionResponseToChannel(ECC_WorldStatic, ECR_Block);
    // //CollisionComp->SetCollisionResponseToChannel(ECC_WorldDynamic, ECR_Block);
    // //CollisionComp->SetCollisionResponseToChannel(ECC_Pawn, ECR_Block);
    // StaticMesh->SetCollisionProfileName(FName("BlockAll"));
    // StaticMesh->BodyInstance.bUseCCD = true;
    // RootComponent = StaticMesh;

}

// Called when the game starts or when spawned
void ABall::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABall::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

